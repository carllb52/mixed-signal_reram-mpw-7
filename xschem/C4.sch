v {xschem version=3.0.0 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 80 -190 130 -190 {
lab=#net1}
N 250 -200 250 -190 {
lab=#net1}
N 130 -190 250 -190 {
lab=#net1}
N 250 -270 250 -260 {
lab=#net1}
N 250 -300 250 -270 {
lab=#net1}
N 250 -200 250 -150 {
lab=#net1}
N 250 -90 250 -50 {
lab=vssa}
N 250 -380 250 -360 {
lab=1v8}
N 430 -160 430 -120 {
lab=vssa}
N 250 -190 390 -190 {
lab=#net1}
N 430 -190 450 -190 {
lab=vssa}
N 450 -190 450 -150 {
lab=vssa}
N 430 -150 450 -150 {
lab=vssa}
N 230 -120 250 -120 {
lab=vssa}
N 230 -120 230 -80 {
lab=vssa}
N 230 -80 250 -80 {
lab=vssa}
N 220 -330 250 -330 {
lab=vssa}
N 430 -360 430 -220 {
lab=vout}
N 430 -460 430 -420 {
lab=1v8}
N 430 -390 460 -390 {
lab=1v8}
N 460 -440 460 -390 {
lab=1v8}
N 430 -440 460 -440 {
lab=1v8}
N 290 -330 430 -330 {
lab=vout}
N 370 -390 390 -390 {
lab=vth}
N 290 -120 320 -120 {
lab=vtl}
N -20 -190 20 -190 {
lab=vin}
N 250 -260 250 -190 {
lab=#net1}
N -110 -480 -90 -480 {
lab=vin}
N -110 -540 -90 -540 {
lab=vth}
N -110 -510 -90 -510 {
lab=vtl}
N -120 -450 -100 -450 {
lab=vout}
N 430 -120 430 -50 {
lab=vssa}
N 250 -50 430 -50 {
lab=vssa}
N 250 -460 430 -460 {
lab=1v8}
N 430 -330 650 -330 {
lab=vout}
N -160 -410 -110 -410 {
lab=1v8}
N -160 -380 -110 -380 {
lab=vssa}
N 250 -460 250 -380 {
lab=1v8}
N 340 -500 340 -460 {
lab=1v8}
N 340 -50 340 -0 {
lab=vssa}
N 130 -250 130 -190 {
lab=#net1}
N 130 -460 130 -310 {
lab=1v8}
N 130 -460 250 -460 {
lab=1v8}
N 570 -270 570 -180 {
lab=vssa}
N 630 -330 790 -330 {
lab=vout}
C {sky130_fd_pr/nfet_01v8.sym} 270 -120 0 1 {name=M2
L=0.15
W=0.42  
nf=1 mult=1
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 270 -330 0 1 {name=M1
L=0.15
W=0.42  
nf=1 mult=1
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 410 -190 0 0 {name=M4
L=0.15
W=0.42 
nf=1 mult=1
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 410 -390 0 0 {name=M3
L=0.15
W=0.42
nf=1 mult=1
model=pfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} 370 -390 0 0 {name=l10 sig_type=std_logic lab=vth}
C {devices/lab_pin.sym} 320 -120 0 1 {name=l11 sig_type=std_logic lab=vtl}
C {devices/lab_pin.sym} -20 -190 0 0 {name=l12 sig_type=std_logic lab=vin}
C {devices/lab_pin.sym} 790 -330 0 1 {name=l13 sig_type=std_logic lab=vout}
C {devices/ipin.sym} -100 -540 0 0 {name=p1 lab=vth}
C {devices/ipin.sym} -100 -510 0 0 {name=p2 lab=vtl}
C {devices/ipin.sym} -100 -480 0 0 {name=p3 lab=vin}
C {devices/opin.sym} -110 -450 0 0 {name=p4 lab=vout}
C {devices/lab_pin.sym} -90 -540 0 1 {name=l8 sig_type=std_logic lab=vth}
C {devices/lab_pin.sym} -90 -510 0 1 {name=l9 sig_type=std_logic lab=vtl}
C {devices/lab_pin.sym} -90 -480 0 1 {name=l14 sig_type=std_logic lab=vin}
C {devices/lab_pin.sym} -120 -450 0 0 {name=l15 sig_type=std_logic lab=vout}
C {devices/iopin.sym} -120 -380 0 0 {name=p5 lab=vssa}
C {devices/iopin.sym} -120 -410 0 0 {name=p6 lab=1v8}
C {sky130_fd_pr/cap_mim_m3_1.sym} 50 -190 1 0 {name=C2 model=cap_mim_m3_1 W=5.5 L=4.4 MF=1 spiceprefix=X}
C {devices/lab_pin.sym} -160 -410 0 0 {name=l1 sig_type=std_logic lab=1v8}
C {devices/lab_pin.sym} -160 -380 0 0 {name=l2 sig_type=std_logic lab=vssa}
C {devices/lab_pin.sym} 340 -500 0 0 {name=l3 sig_type=std_logic lab=1v8}
C {devices/lab_pin.sym} 340 0 0 0 {name=l6 sig_type=std_logic lab=vssa}
C {devices/lab_pin.sym} 220 -330 0 0 {name=l7 sig_type=std_logic lab=vssa}
C {sky130_fd_pr/cap_mim_m3_1.sym} 130 -280 0 0 {name=C1 model=cap_mim_m3_1 W=5.5 L=4.4 MF=1 spiceprefix=X}
C {sky130_fd_pr/cap_mim_m3_1.sym} 570 -300 0 0 {name=C3 model=cap_mim_m3_1 W=3.22 L=4.38 MF=1 spiceprefix=X}
C {devices/lab_pin.sym} 570 -180 0 0 {name=l4 sig_type=std_logic lab=vssa}
