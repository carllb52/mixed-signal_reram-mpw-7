v {xschem version=3.0.0 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N -580 -380 -550 -380 { lab=io_analog[4]}
N -510 -540 -510 -450 { lab=io_analog[0]}
N -510 -450 -510 -440 { lab=io_analog[0]}
N -430 -510 -430 -440 { lab=io_analog[1]}
N -430 -540 -430 -510 { lab=io_analog[1]}
N -390 -340 -350 -340 { lab=vssa1}
N -580 -340 -550 -340 { lab=io_analog[5]}
N -510 -280 -510 -260 {
lab=io_analog[2]}
N -430 -280 -430 -260 {
lab=io_analog[3]}
N -430 -1030 -380 -1030 { lab=vssa2}
N -430 -940 -380 -940 { lab=io_analog[9]}
N -610 -990 -560 -990 { lab=io_analog[8]}
N -480 -1080 -480 -1040 { lab=io_analog[10]}
N 120 -1020 140 -1020 {
lab=gpio_analog[1]}
N 120 -1000 140 -1000 {
lab=gpio_analog[2]}
N 120 -980 140 -980 {
lab=io_analog[6]}
N 440 -1020 480 -1020 {
lab=#net1}
N 440 -1000 480 -1000 {
lab=vccd1}
N 440 -980 480 -980 {
lab=vssd1}
N 1090 -1159 1090 -1059 {
lab=vccd1}
N 1090 -939 1090 -879 {
lab=vssd1}
N 1160 -999 1230 -999 {
lab=gpio_noesd[0]}
N 910 -1019 1000 -1019 {
lab=#net1}
N 910 -979 1000 -979 {
lab=gpio_noesd[0]}
N 357 -497 357 -452 { lab=#net2}
N 222 -372 267 -372 { lab=gpio_analog[11]}
N 327 -452 327 -422 { lab=gpio_analog[9]}
N 387 -372 437 -372 { lab=gpio_analog[12]}
N 480 -1020 845 -1020 {
lab=#net1}
N 845 -1020 910 -1020 {
lab=#net1}
N 910 -1020 910 -1019 {
lab=#net1}
N 357 -292 357 -226 {
lab=#net3}
N 327 -322 327 -291 {
lab=gpio_analog[10]}
N 1090 -319 1090 -259 {
lab=vssd2}
N 1090 -539 1090 -439 {
lab=vccd2}
C {devices/lab_pin.sym} -430 -540 2 0 {name=l1 lab=io_analog[1]}
C {devices/lab_pin.sym} -510 -540 0 0 {name=l2 lab=io_analog[0]}
C {devices/lab_pin.sym} -580 -380 0 0 {name=l3 lab=io_analog[4]}
C {devices/lab_pin.sym} -580 -340 0 0 {name=l4 lab=io_analog[5]}
C {devices/lab_pin.sym} -510 -260 0 0 {name=l5 lab=io_analog[2]}
C {devices/lab_pin.sym} -430 -260 2 0 {name=l6 lab=io_analog[3]}
C {devices/lab_pin.sym} -355 -340 2 0 {name=l9 lab=vssa1}
C {1T1R_2x2.sym} -470 -360 0 0 {name=x1}
C {devices/iopin.sym} -1080 -1140 0 0 {name=p7 lab=vdda1}
C {devices/iopin.sym} -1080 -1110 0 0 {name=p8 lab=vdda2}
C {devices/iopin.sym} -1080 -1080 0 0 {name=p10 lab=vssa1}
C {devices/iopin.sym} -1080 -1050 0 0 {name=p11 lab=vssa2}
C {devices/iopin.sym} -1080 -1020 0 0 {name=p12 lab=vccd1}
C {devices/iopin.sym} -1080 -990 0 0 {name=p13 lab=vccd2}
C {devices/iopin.sym} -1080 -960 0 0 {name=p14 lab=vssd1}
C {devices/iopin.sym} -1080 -930 0 0 {name=p15 lab=vssd2}
C {devices/ipin.sym} -1030 -860 0 0 {name=p16 lab=wb_clk_i}
C {devices/ipin.sym} -1030 -830 0 0 {name=p17 lab=wb_rst_i}
C {devices/ipin.sym} -1030 -800 0 0 {name=p18 lab=wbs_stb_i}
C {devices/ipin.sym} -1030 -770 0 0 {name=p19 lab=wbs_cyc_i}
C {devices/ipin.sym} -1030 -740 0 0 {name=p20 lab=wbs_we_i}
C {devices/ipin.sym} -1030 -710 0 0 {name=p21 lab=wbs_sel_i[3:0]}
C {devices/ipin.sym} -1030 -680 0 0 {name=p22 lab=wbs_dat_i[31:0]}
C {devices/ipin.sym} -1030 -650 0 0 {name=p23 lab=wbs_adr_i[31:0]}
C {devices/opin.sym} -1040 -590 0 0 {name=p24 lab=wbs_ack_o}
C {devices/opin.sym} -1040 -560 0 0 {name=p25 lab=wbs_dat_o[31:0]}
C {devices/ipin.sym} -1030 -520 0 0 {name=p26 lab=la_data_in[127:0]}
C {devices/opin.sym} -1040 -490 0 0 {name=p27 lab=la_data_out[127:0]}
C {devices/ipin.sym} -1030 -410 0 0 {name=p28 lab=io_in[26:0]}
C {devices/ipin.sym} -1030 -380 0 0 {name=p29 lab=io_in_3v3[26:0]}
C {devices/ipin.sym} -1040 -100 0 0 {name=p30 lab=user_clock2}
C {devices/opin.sym} -1040 -350 0 0 {name=p31 lab=io_out[26:0]}
C {devices/opin.sym} -1040 -320 0 0 {name=p32 lab=io_oeb[26:0]}
C {devices/iopin.sym} -1070 -260 0 0 {name=p33 lab=gpio_analog[17:0]}
C {devices/iopin.sym} -1070 -230 0 0 {name=p34 lab=gpio_noesd[17:0]}
C {devices/iopin.sym} -1070 -200 0 0 {name=p35 lab=io_analog[10:0]}
C {devices/iopin.sym} -1070 -170 0 0 {name=p36 lab=io_clamp_high[2:0]}
C {devices/iopin.sym} -1070 -140 0 0 {name=p37 lab=io_clamp_low[2:0]}
C {devices/opin.sym} -1050 -70 0 0 {name=p38 lab=user_irq[2:0]}
C {devices/ipin.sym} -1030 -460 0 0 {name=p39 lab=la_oenb[127:0]}
C {FG_pfet.sym} -410 -970 0 0 {name=x4}
C {devices/lab_pin.sym} -380 -1030 2 0 {name=l19 lab=vssa2}
C {devices/lab_pin.sym} -380 -940 2 0 {name=l20 lab=io_analog[9]}
C {devices/lab_pin.sym} -610 -990 0 0 {name=l21 lab=io_analog[8]}
C {devices/lab_pin.sym} -480 -1080 1 0 {name=l22 lab=io_analog[10]}
C {C4.sym} 290 -1000 0 0 {name=x5}
C {devices/lab_pin.sym} 480 -1000 0 1 {name=l23 sig_type=std_logic lab=vccd1}
C {devices/lab_pin.sym} 480 -980 0 1 {name=l24 sig_type=std_logic lab=vssd1
}
C {devices/lab_pin.sym} 120 -1020 0 0 {name=l27 sig_type=std_logic lab=gpio_analog[1]
}
C {devices/lab_pin.sym} 120 -1000 0 0 {name=l25 sig_type=std_logic lab=gpio_analog[2]
}
C {devices/lab_pin.sym} 1230 -999 2 0 {name=l28 sig_type=std_logic lab=gpio_noesd[0]}
C {sky130_sc_ams__ota_1.sym} 1080 -999 0 0 {name=x3}
C {devices/lab_pin.sym} 1090 -879 0 0 {name=l17 sig_type=std_logic lab=vssd1}
C {hv_tgate.sym} 327 -372 0 0 {name=x8}
C {devices/lab_pin.sym} 357.1199999999999 -497.6099999999999 3 1 {name=l49 lab=vdda1}
C {devices/lab_pin.sym} 357.3599999999999 -226.63 0 1 {name=l50 lab=vssa1}
C {devices/lab_pin.sym} 222 -372 0 0 {name=l51 sig_type=std_logic lab=gpio_analog[11]}
C {devices/lab_pin.sym} 327 -452 1 0 {name=l52 sig_type=std_logic lab=gpio_analog[9]}
C {devices/lab_pin.sym} 327 -292 3 0 {name=l53 sig_type=std_logic lab=gpio_analog[10]}
C {devices/lab_pin.sym} 437 -372 2 0 {name=l54 sig_type=std_logic lab=gpio_analog[12]}
C {devices/lab_pin.sym} 1090 -1159 0 0 {name=l55 sig_type=std_logic lab=vccd1
}
C {devices/lab_pin.sym} 120 -980 0 0 {name=l8 sig_type=std_logic lab=io_analog[6]}
C {sky130_sc_ams__ota_1.sym} 1080 -379 0 0 {name=x2}
C {devices/lab_pin.sym} 1090 -259 0 0 {name=l10 sig_type=std_logic lab=vssd2}
C {devices/lab_pin.sym} 1090 -539 0 0 {name=l11 sig_type=std_logic lab=vccd2
}
C {devices/lab_pin.sym} 910 -979 2 1 {name=l7 sig_type=std_logic lab=gpio_noesd[0]}
C {devices/lab_pin.sym} 1000 -399 0 0 {name=l12 sig_type=std_logic lab=gpio_noesd[13]}
C {devices/lab_pin.sym} 1000 -359 0 0 {name=l13 sig_type=std_logic lab=gpio_noesd[14]}
C {devices/lab_pin.sym} 1160 -379 0 1 {name=l14 sig_type=std_logic lab=gpio_noesd[15]}
