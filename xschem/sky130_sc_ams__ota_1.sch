v {xschem version=3.0.0 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N -250 -180 -250 -120 { lab=#net1}
N -350 -210 -290 -210 { lab=#net1}
N -290 -160 -250 -160 { lab=#net1}
N 270 -180 270 -120 { lab=#net2}
N 310 -210 370 -210 { lab=#net2}
N -310 -160 -290 -160 { lab=#net1}
N -310 -210 -310 -160 { lab=#net1}
N 270 -160 330 -160 { lab=#net2}
N 330 -210 330 -160 { lab=#net2}
N -380 -240 300 -240 { lab=VPWR}
N -560 90 -560 120 { lab=#net3}
N -1060 0 -1020 0 { lab=#net4}
N -1020 0 -1020 30 { lab=#net4}
N -1140 60 -1060 60 { lab=VGND}
N -0 -60 0 -0 { lab=#net5}
N -560 -180 -560 90 { lab=#net3}
N -1020 30 -960 30 { lab=#net4}
N -1020 -210 -960 -210 { lab=#net6}
N -960 -160 -920 -160 { lab=#net6}
N -980 -160 -960 -160 { lab=#net6}
N -980 -210 -980 -160 { lab=#net6}
N -420 30 -40 30 { lab=#net4}
N -1060 -180 -1060 0 { lab=#net4}
N -920 -180 -920 0 { lab=#net6}
N -1220 -100 -1220 -40 { lab=#net4}
N -1220 -40 -1060 -40 { lab=#net4}
N -1220 -160 -980 -160 { lab=#net6}
N -1360 -240 -1060 -240 { lab=VPWR}
N -1360 -180 -1360 -130 { lab=#net7}
N -1360 -130 -1260 -130 { lab=#net7}
N -1360 -180 -1320 -180 { lab=#net7}
N -1320 -210 -1320 -180 { lab=#net7}
N -1360 -130 -1360 0 { lab=#net7}
N -1360 60 -1140 60 { lab=VGND}
N -1320 0 -1320 30 { lab=#net4}
N -1320 0 -1060 0 { lab=#net4}
N -1060 -240 -1000 -240 { lab=VPWR}
N -720 120 -560 120 { lab=#net3}
N -680 150 -680 180 { lab=VGND}
N -720 180 -680 180 { lab=VGND}
N -490 -60 -450 -60 { lab=VGND}
N 170 -240 170 -210 { lab=VPWR}
N 130 -180 130 -160 { lab=#net2}
N 410 -180 410 120 { lab=Iout}
N 130 -160 270 -160 { lab=#net2}
N -150 -240 -150 -210 { lab=VPWR}
N -110 -180 -110 -160 { lab=#net1}
N -250 -160 -110 -160 { lab=#net1}
N 300 -240 410 -240 { lab=VPWR}
N -490 -90 -490 -60 { lab=VGND}
N -450 -120 -250 -120 { lab=#net1}
N -250 -60 270 -60 { lab=#net5}
N -520 150 370 150 { lab=#net3}
N -560 100 -500 100 { lab=#net3}
N -500 100 -500 150 { lab=#net3}
N -640 -240 -380 -240 { lab=VPWR}
N -520 -210 -350 -210 { lab=#net1}
N -1170 30 -1170 60 { lab=VGND}
N -1060 -40 -770 -40 { lab=#net4}
N -1000 -240 -940 -240 { lab=VPWR}
N -830 -240 -830 -210 { lab=VPWR}
N -790 -180 -790 -160 { lab=#net6}
N -920 -160 -790 -160 { lab=#net6}
N -920 0 -780 0 { lab=#net6}
N -820 30 -820 60 { lab=VGND}
N -820 60 -780 60 { lab=VGND}
N -780 60 -780 120 { lab=VGND}
N -940 -240 -640 -240 { lab=VPWR}
N -770 -40 -660 -40 { lab=#net4}
N -660 -40 -660 30 { lab=#net4}
N -660 30 -420 30 { lab=#net4}
N -920 120 -780 120 { lab=VGND}
C {devices/ipin.sym} 310 -90 0 1 {name=Vin1 lab=Vin1}
C {devices/ipin.sym} -290 -90 0 0 {name=Vin2 lab=Vin2}
C {devices/ipin.sym} 0 -380 0 0 {name=p3 lab=VGND}
C {devices/ipin.sym} 0 -410 0 0 {name=p4 lab=VPWR}
C {devices/opin.sym} 410 -70 0 0 {name=Iout lab=Iout}
C {sky130_fd_pr/pfet_01v8.sym} 390 -210 0 0 {name=M7
L=0.30
W=0.61
nf=1
mult=4
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} -540 -210 0 1 {name=M8
L=0.15
W=0.61
nf=1
mult=4
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 290 -210 0 1 {name=M9
L=0.15
W=0.61
nf=1
mult=4
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} -20 30 0 0 {name=M1
L=0.15
W=0.70
nf=1 
mult=4
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} -1040 30 0 1 {name=M10
L=0.15
W=0.70
nf=1 
mult=4
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} -940 30 0 0 {name=M11
L=0.15
W=0.70
nf=1 
mult=8
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} -940 -210 0 0 {name=M12
L=0.15
W=0.61
nf=1
mult=4
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} -1040 -210 0 1 {name=M13
L=0.15
W=0.61
nf=1
mult=4
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} -1240 -130 0 0 {name=M14
L=0.15
W=0.70
nf=1 
mult=4
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} -1340 30 0 1 {name=M16
L=0.15
W=0.70
nf=1 
mult=4
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} -270 -90 0 0 {name=M2
L=0.15
W=1.06
nf=1 
mult=4
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 290 -90 0 1 {name=M3
L=0.15
W=1.06
nf=1 
mult=4
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} -700 150 0 1 {name=M17
L=0.15
W=0.70
nf=1 
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} -470 -90 0 0 {name=M18
L=0.15
W=1.06
nf=1 
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 150 -210 0 1 {name=M19
L=0.15
W=0.61
nf=1
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} -130 -210 0 0 {name=M20
L=0.15
W=0.61
nf=1
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} -540 150 0 1 {name=M4
L=0.15
W=0.70
nf=1 
mult=4
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} -110 -210 0 1 {name=l10 sig_type=std_logic lab=VPWR}
C {devices/lab_pin.sym} -250 -210 0 1 {name=l11 sig_type=std_logic lab=VPWR}
C {devices/lab_pin.sym} 410 -210 0 1 {name=l12 sig_type=std_logic lab=VPWR}
C {devices/lab_pin.sym} 130 -210 0 0 {name=l13 sig_type=std_logic lab=VPWR}
C {devices/lab_pin.sym} 270 -210 0 0 {name=l14 sig_type=std_logic lab=VPWR}
C {devices/lab_pin.sym} -560 -210 0 0 {name=l15 sig_type=std_logic lab=VPWR}
C {devices/lab_pin.sym} -1060 -210 0 0 {name=l16 sig_type=std_logic lab=VPWR}
C {devices/lab_pin.sym} -920 -210 0 1 {name=l18 sig_type=std_logic lab=VPWR}
C {devices/lab_pin.sym} -250 -90 0 1 {name=l20 sig_type=std_logic lab=VGND}
C {devices/lab_pin.sym} -450 -90 0 1 {name=l21 sig_type=std_logic lab=VGND}
C {devices/lab_pin.sym} 270 -90 0 0 {name=l22 sig_type=std_logic lab=VGND}
C {devices/lab_pin.sym} 0 30 0 1 {name=l23 sig_type=std_logic lab=VGND}
C {devices/lab_pin.sym} 410 150 0 1 {name=l24 sig_type=std_logic lab=VGND}
C {devices/lab_pin.sym} -560 150 0 0 {name=l25 sig_type=std_logic lab=VGND}
C {devices/lab_pin.sym} -720 150 0 0 {name=l26 sig_type=std_logic lab=VGND}
C {devices/lab_pin.sym} -920 30 0 1 {name=l27 sig_type=std_logic lab=VGND}
C {devices/lab_pin.sym} -1060 30 0 0 {name=l28 sig_type=std_logic lab=VGND}
C {devices/lab_pin.sym} -1360 30 0 0 {name=l29 sig_type=std_logic lab=VGND}
C {devices/lab_pin.sym} -1220 -130 0 1 {name=l30 sig_type=std_logic lab=VGND}
C {sky130_fd_pr/nfet_01v8.sym} -1190 30 0 1 {name=M21
L=0.15
W=0.70
nf=1 
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} -1210 30 0 0 {name=l31 sig_type=std_logic lab=VGND}
C {sky130_fd_pr/pfet_01v8.sym} -810 -210 0 0 {name=M22
L=0.15
W=0.61
nf=1
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} -790 -210 0 1 {name=l32 sig_type=std_logic lab=VPWR}
C {sky130_fd_pr/pfet_01v8.sym} -1340 -210 0 1 {name=M15
L=4.20
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} -1360 -210 0 0 {name=l33 sig_type=std_logic lab=VPWR}
C {sky130_fd_pr/nfet_01v8.sym} -800 30 0 0 {name=M23
L=0.15
W=0.70
nf=1 
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} -780 30 0 1 {name=l17 sig_type=std_logic lab=VGND}
C {devices/lab_pin.sym} -940 90 0 0 {name=l34 sig_type=std_logic lab=VGND}
C {sky130_fd_pr/res_xhigh_po.sym} -920 90 0 0 {name=R2
W=1.41
L=0.70
model=res_xhigh_po
spiceprefix=X
mult=1}
C {sky130_fd_pr/pfet_01v8.sym} -270 -210 0 0 {name=M6
L=0.15
W=0.61
nf=1
mult=4
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 390 150 0 0 {name=M5
L=0.30
W=0.70
nf=1 
mult=4
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} 0 -380 0 1 {name=l1 sig_type=std_logic lab=VGND}
C {devices/lab_pin.sym} 0 -410 0 1 {name=l5 sig_type=std_logic lab=VPWR}
C {devices/lab_pin.sym} 0 60 3 0 {name=l35 sig_type=std_logic lab=VGND}
C {devices/lab_pin.sym} 0 -240 3 1 {name=l36 sig_type=std_logic lab=VPWR}
C {devices/lab_pin.sym} -560 180 3 0 {name=l2 sig_type=std_logic lab=VGND}
C {devices/lab_pin.sym} 410 180 3 0 {name=l3 sig_type=std_logic lab=VGND}
C {devices/lab_pin.sym} -720 180 3 0 {name=l8 sig_type=std_logic lab=VGND}
C {devices/lab_pin.sym} -920 120 3 0 {name=l6 sig_type=std_logic lab=VGND}
C {devices/lab_pin.sym} -1210 60 3 0 {name=l4 sig_type=std_logic lab=VGND}
C {devices/lab_pin.sym} -450 -60 3 0 {name=l9 sig_type=std_logic lab=VGND}
