v {xschem version=3.0.0 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
C {devices/code_shown.sym} -85 -75 0 0 {name=s1 only_toplevel=false value="X0 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=1.7864e+14p pd=1.75523e+09u as=0p ps=0u w=870000u l=4.73e+06u
X1 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.05e+06u
X2 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X3 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=1.14715e+14p pd=1.27912e+09u as=0p ps=0u w=550000u l=4.73e+06u
X4 VPWR _148_/A a_14025_8797# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X5 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X6 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X7 VPWR _134_/X a_12119_7439# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X8 VGND _098_/D_N a_10515_5737# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X9 chan[8].tgate_inst/A_NOT a_10055_15823# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X10 a_14216_8751# _132_/C VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X11 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X12 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X13 VPWR _142_/A a_10791_4943# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X14 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X15 _161_/A a_13257_5853# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X16 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X17 a_12683_6549# _122_/X VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X18 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X19 VPWR _148_/A a_11329_8751# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X20 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X21 chan[2].tgate_inst/A _132_/A VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X22 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X23 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X24 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X25 a_12143_9991# _148_/D_N VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X26 a_11163_9411# a_11057_9411# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X27 chan[4].tgate_inst/A _148_/A VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X28 a[12] chan[12].tgate_inst/A_NOT com VPWR sky130_fd_pr__pfet_01v8 ad=7.168e+11p pd=5.68e+06u as=1.14688e+13p ps=9.088e+07u w=2.56e+06u l=150000u
X29 a_11475_5533# a_11421_5639# a_11375_5533# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X30 a_13977_6263# _093_/A VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X31 VPWR a_11991_6005# a_11522_6263# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X32 _155_/A a_12337_8029# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X33 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X34 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X35 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X36 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X37 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X38 VGND _108_/A a_13183_8207# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X39 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X40 VGND a_2695_16911# input3/X VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X41 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X42 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X43 a_12205_7439# a_11803_7119# a_12119_7439# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X44 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X45 a_12451_5461# _139_/X VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X46 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X47 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X48 VPWR _143_/X a_11803_7119# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X49 a_11057_9411# _090_/D_N VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X50 VGND _105_/A a_11930_8207# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X51 a_11706_6727# a_12175_6549# a_12119_6621# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X52 com chan[12].tgate_inst/A a[12] VGND sky130_fd_pr__nfet_01v8 ad=1.14688e+13p pd=9.088e+07u as=7.168e+11p ps=5.68e+06u w=2.56e+06u l=150000u
X53 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.97e+06u
X54 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X55 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X56 VGND _139_/X a_12723_7119# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X57 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X58 _148_/D_N a_11930_8207# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X59 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X60 chan[11].tgate_inst/A_NOT a_13841_9615# a_14223_9615# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X61 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X62 a_9561_8439# _108_/A VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X63 chan[11].tgate_inst/A a_13735_10927# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X64 chan[8].tgate_inst/A _148_/B VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X65 VPWR a_14025_8797# _124_/Y VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X66 a_10057_6825# a_9595_6938# a_9961_6941# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X67 VGND _107_/A a_9819_6005# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X68 VGND _148_/D_N a_12249_8207# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X69 VGND _119_/A a_12723_12559# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X70 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X71 a_13261_6575# _143_/X a_13173_6575# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X72 _121_/A a_9319_7119# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X73 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X74 VPWR _090_/D_N a_10765_5737# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X75 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X76 a_10129_9001# _098_/D_N a_10057_9001# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X77 VGND _154_/B a_13367_6575# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X78 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X79 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X80 _134_/X a_11251_8207# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X81 _117_/C a_13183_8207# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X82 VPWR a_12919_7119# a_13039_7439# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X83 VPWR _101_/X a_10055_3311# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X84 VGND _090_/D_N a_10799_7119# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X85 _101_/X a_9879_6941# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X86 VGND _148_/A a_11329_8751# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X87 _129_/A a_10515_5737# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X88 VPWR _088_/A a_10642_6031# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X89 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X90 VPWR _148_/A a_12393_10089# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X91 a_12143_9991# _148_/D_N VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X92 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X93 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X94 _131_/Y _132_/C VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X95 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.05e+06u
X96 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X97 a_12709_8207# a_12532_8207# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X98 a_9275_7815# a_9539_7815# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X99 VPWR _088_/A a_10826_6575# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X100 VGND _142_/A a_10791_4943# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X101 a_11421_6031# a_11244_6031# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X102 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X103 chan[11].tgate_inst/A a_13735_10927# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X104 _124_/Y _148_/B VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X105 a_12191_5533# _158_/B VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X106 _161_/X a_14103_6575# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X107 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X108 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X109 VPWR _098_/A a_11010_8751# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X110 VPWR _090_/D_N a_11049_7235# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X111 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X112 VGND a_13551_8207# _148_/C VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X113 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X114 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X115 VPWR _107_/A a_10961_6031# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X116 VPWR a_11706_6727# _144_/X VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X117 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X118 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X119 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X120 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X121 a[2] chan[2].tgate_inst/A_NOT com VPWR sky130_fd_pr__pfet_01v8 ad=7.168e+11p pd=5.68e+06u as=0p ps=0u w=2.56e+06u l=150000u
X122 a_11575_7338# _093_/A VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X123 a_13691_7338# _125_/A VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X124 a_11062_5639# a_11531_5461# a_11475_5533# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X125 VPWR _139_/X a_12447_6031# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X126 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X127 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X128 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X129 chan[2].tgate_inst/A a_14514_9372# a_14873_9295# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X130 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X131 VPWR a_9183_6250# _104_/X VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X132 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X133 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X134 a_12523_8751# _148_/B a_12333_8751# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X135 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X136 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X137 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X138 a_9961_6941# a_9773_6621# a_9879_6941# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X139 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X140 VPWR _125_/A a_13183_9295# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X141 a_14116_9615# _117_/C a_14032_9615# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X142 a_9539_7815# _108_/A VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X143 VGND _117_/C _131_/Y VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X144 a_11877_5309# _158_/B a_11777_5309# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X145 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X146 a_12291_7439# a_11999_7119# a_12205_7439# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X147 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X148 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X149 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X150 VPWR a_11506_8751# a_11612_8751# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X151 VPWR a_13622_8751# a_13728_8751# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X152 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X153 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.05e+06u
X154 VPWR _148_/C _124_/Y VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X155 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X156 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.05e+06u
X157 a_12919_7119# _143_/X VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X158 a_10471_6740# _088_/A VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X159 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X160 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.97e+06u
X161 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X162 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X163 VGND a_9183_6250# _104_/X VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X164 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X165 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X166 a_9735_9527# _090_/C VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X167 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X168 VPWR _140_/X a_13257_5853# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X169 chan[2].tgate_inst/A _132_/C VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X170 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X171 _091_/A a_11163_9411# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X172 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X173 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.05e+06u
X174 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X175 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.05e+06u
X176 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X177 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.05e+06u
X178 a_11915_6621# _134_/X VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X179 a[8] chan[8].tgate_inst/A_NOT com VPWR sky130_fd_pr__pfet_01v8 ad=7.168e+11p pd=5.68e+06u as=0p ps=0u w=2.56e+06u l=150000u
X180 com chan[10].tgate_inst/A a[10] VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=7.168e+11p ps=5.68e+06u w=2.56e+06u l=150000u
X181 VGND _090_/B a_11527_8207# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X182 VGND _121_/A a_8767_6938# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X183 VPWR _107_/A a_9819_6005# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X184 VPWR _154_/B a_11982_5639# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X185 a_10693_5737# _098_/D_N a_10597_5737# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X186 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X187 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X188 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X189 VGND _098_/A a_11010_8751# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X190 VGND a_13694_6263# a_13643_6031# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X191 VGND a_11522_6263# _137_/A VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X192 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.05e+06u
X193 VGND _121_/A a_9595_6938# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X194 VGND a_9275_7815# _097_/A VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X195 VGND _101_/X a_10055_3311# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X196 VPWR a_11023_4564# _151_/X VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X197 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X198 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X199 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X200 a_10563_7815# a_10927_7643# a_10862_7913# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X201 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X202 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X203 VPWR _091_/A a_14103_10927# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X204 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X205 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X206 VPWR a_1407_12015# input2/X VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X207 VGND _125_/A a_11145_6575# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X208 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.97e+06u
X209 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X210 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X211 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X212 _116_/A a_9879_9117# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X213 VGND _154_/B a_13311_7439# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X214 _161_/X a_14103_6575# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X215 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X216 chan[11].tgate_inst/A_NOT _148_/B VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X217 VPWR _108_/A a_13257_5853# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X218 chan[10].tgate_inst/A_NOT a_14287_15279# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X219 a_10195_8426# input2/X VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X220 VPWR _133_/X a_12171_7663# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X221 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X222 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=2.89e+06u
X223 a_13341_7913# _148_/C a_13091_7913# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X224 _132_/C a_12683_6549# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X225 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.05e+06u
X226 VGND a_11506_8751# a_11612_8751# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X227 VGND a_13622_8751# a_13728_8751# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X228 a_13091_6575# _140_/X VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X229 VGND a_10099_9385# a_9735_9527# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X230 VGND _132_/A a_14669_8797# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X231 VPWR a_10287_5652# chan[6].tgate_inst/A VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X232 a[4] chan[4].tgate_inst/A_NOT com VPWR sky130_fd_pr__pfet_01v8 ad=7.168e+11p pd=5.68e+06u as=0p ps=0u w=2.56e+06u l=150000u
X233 VGND _090_/B a_10563_8439# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X234 _098_/D_N a_9687_7119# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X235 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X236 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X237 VGND _091_/A a_14103_10927# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X238 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X239 VGND _125_/A a_13183_9295# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X240 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X241 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X242 chan[12].tgate_inst/A a_14103_12015# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X243 VPWR _148_/D_N _126_/Y VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X244 _130_/Y _148_/A VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X245 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X246 VPWR _140_/X a_11706_6727# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X247 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X248 a_11322_6575# a_11145_6575# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X249 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X250 VGND a_9595_9114# a_9879_9117# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X251 a_10471_6740# _088_/A VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X252 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X253 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X254 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X255 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X256 a_11522_6263# _158_/B VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X257 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X258 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X259 VPWR a_9880_8439# _108_/A VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X260 _147_/A a_11601_4943# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X261 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X262 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=2.89e+06u
X263 VPWR a_10563_8439# _086_/X VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X264 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X265 a_11705_5309# a_11435_4943# a_11601_4943# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X266 VGND _143_/X a_13091_5487# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X267 VGND _122_/X a_11421_5639# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X268 _131_/Y a_13758_7637# a_13538_7913# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X269 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X270 a_12434_9295# a_12654_9269# chan[4].tgate_inst/A VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X271 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X272 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X273 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.05e+06u
X274 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X275 VPWR _148_/D_N a_11895_8751# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X276 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X277 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X278 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X279 a_9879_6941# _105_/A VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X280 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X281 VPWR a_7987_6740# chan[3].tgate_inst/A VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X282 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.97e+06u
X283 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.05e+06u
X284 VGND _133_/X a_12171_7663# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X285 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X286 a_10515_5737# _152_/C VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X287 VPWR a_11062_5639# _142_/A VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X288 VPWR a_9459_6263# _113_/X VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X289 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X290 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X291 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X292 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X293 a_10789_7913# _125_/A a_10698_7913# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X294 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X295 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X296 chan[15].tgate_inst/A_NOT _132_/A VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X297 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X298 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X299 a_15051_8751# _117_/A_N a_14944_8751# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X300 _119_/A a_10799_7119# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X301 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X302 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X303 VGND _098_/D_N a_9459_6263# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X304 a_12175_6549# _143_/X VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X305 a_14177_7439# _148_/C a_14093_7439# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X306 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X307 chan[4].tgate_inst/A a_12654_9269# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X308 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X309 a[3] chan[3].tgate_inst/A_NOT com VPWR sky130_fd_pr__pfet_01v8 ad=7.168e+11p pd=5.68e+06u as=0p ps=0u w=2.56e+06u l=150000u
X310 a_9051_6941# a_8945_6621# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X311 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.97e+06u
X312 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X313 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X314 a_10799_7119# a_10693_7439# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X315 input2/X a_1407_12015# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X316 _130_/Y _148_/D_N a_14283_8207# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X317 VPWR _117_/C a_13091_7913# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X318 VGND a_13977_6263# a_13790_6005# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X319 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X320 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X321 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X322 a_11987_9295# _148_/A VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X323 VGND input2/X a_11251_8207# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X324 _153_/A a_12119_7439# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X325 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X326 _103_/X a_9051_6941# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X327 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X328 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X329 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X330 VGND _108_/A a_13533_5487# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X331 VPWR _154_/B a_13091_6575# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X332 VGND _107_/A a_12815_8207# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X333 VPWR a_12815_8207# _148_/B VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X334 VGND a_10287_5652# chan[6].tgate_inst/A VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X335 a_9723_6121# _152_/C VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X336 a_11601_4943# a_11435_4943# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X337 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X338 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.97e+06u
X339 a_11049_7235# _098_/B a_10977_7235# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X340 a[1] _087_/X com VPWR sky130_fd_pr__pfet_01v8 ad=7.168e+11p pd=5.68e+06u as=0p ps=0u w=2.56e+06u l=150000u
X341 a_11375_5533# _140_/X a_11271_5533# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X342 chan[12].tgate_inst/A a_14103_12015# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X343 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X344 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X345 VGND _107_/A a_12613_7663# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X346 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.05e+06u
X347 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X348 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X349 VGND a_9735_9527# _099_/A VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X350 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X351 a_12083_8751# a_11895_8751# _109_/Y VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X352 a_12119_7439# _152_/C VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X353 VPWR _090_/D_N a_10129_6825# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X354 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X355 VGND _148_/C chan[8].tgate_inst/A VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X356 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.97e+06u
X357 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X358 VGND _148_/D_N a_11895_8751# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X359 VPWR a_9561_8439# a_9374_8181# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X360 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X361 VPWR a_10195_8426# _090_/D_N VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X362 a_10563_8916# input2/X VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X363 VPWR a_11667_7828# _105_/A VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X364 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X365 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X366 VGND _148_/C _131_/Y VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X367 a_10765_5737# _125_/A a_10693_5737# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X368 a_13367_6575# _140_/X a_13261_6575# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X369 a_11706_6727# a_12065_6727# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X370 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X371 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X372 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.97e+06u
X373 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.05e+06u
X374 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X375 a_11531_5461# _139_/X VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X376 a_11935_6351# a_11881_6263# a_11835_6351# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X377 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X378 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X379 _148_/B a_12815_8207# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X380 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X381 VPWR a_9735_5162# _114_/X VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X382 a_11605_6575# a_11428_6575# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X383 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X384 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X385 VPWR _105_/A a_9595_9114# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X386 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.05e+06u
X387 VPWR _158_/X a_13827_4943# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X388 VGND _152_/C a_12889_6397# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X389 VPWR sel[1] a_1407_12015# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X390 VGND a_7987_6740# chan[3].tgate_inst/A VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X391 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X392 VPWR _155_/A a_13735_10927# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X393 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X394 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X395 a_13783_6740# _093_/A VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X396 chan[15].tgate_inst/A_NOT _132_/C VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X397 a_10563_8916# input2/X VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X398 VGND a_11982_5639# _150_/X VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X399 a_9735_9527# _098_/A VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X400 VPWR a_13231_6250# _152_/C VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X401 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X402 VGND a_10927_8297# a_10563_8439# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X403 VGND input3/X a_10147_7119# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X404 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X405 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X406 chan[4].tgate_inst/A _132_/C VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X407 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X408 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X409 VGND a_8767_6938# a_9051_6941# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X410 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X411 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X412 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.05e+06u
X413 a_12613_6031# _140_/X VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X414 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.97e+06u
X415 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X416 VPWR _098_/B a_10927_8297# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X417 VPWR _108_/A a_13183_8207# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X418 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X419 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.97e+06u
X420 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X421 VGND _155_/A a_13735_10927# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X422 VPWR _132_/A a_14669_8797# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X423 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X424 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X425 a_14223_9615# _148_/D_N a_14116_9615# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X426 VGND a_13758_7637# _131_/Y VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X427 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X428 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X429 VPWR _105_/A a_11930_8207# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X430 VGND a_13231_6250# _152_/C VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X431 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X432 a_14860_8751# _117_/C VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X433 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.05e+06u
X434 a_10515_5737# _125_/A VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X435 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=2.89e+06u
X436 VPWR _122_/X a_11881_6263# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X437 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X438 VPWR _125_/A a_11413_9411# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X439 VPWR _139_/X a_12723_7119# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X440 a_11062_5639# a_11421_5639# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X441 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X442 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.05e+06u
X443 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X444 VPWR _163_/A a_14195_7663# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X445 _158_/B a_11159_4943# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X446 _129_/X a_14195_5487# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X447 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X448 VPWR _125_/A a_11145_6575# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X449 VGND _148_/C chan[2].tgate_inst/A VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X450 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X451 VPWR _148_/D_N a_12249_8207# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X452 chan[15].tgate_inst/A_NOT _117_/C a_14261_7439# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X453 _090_/B a_10607_9295# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X454 a_11777_5309# _133_/X a_11705_5309# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X455 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X456 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.05e+06u
X457 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X458 VGND a_11667_7828# _105_/A VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X459 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X460 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=2.89e+06u
X461 a_10597_5737# _152_/C a_10515_5737# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X462 a_14296_6263# sel[3] VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X463 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X464 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X465 _117_/C a_13183_8207# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X466 com chan[2].tgate_inst/A a[2] VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=7.168e+11p ps=5.68e+06u w=2.56e+06u l=150000u
X467 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X468 a_11245_9411# a_11057_9411# a_11163_9411# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X469 VPWR a_9183_8916# chan[3].tgate_inst/A_NOT VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X470 VGND a_9735_5162# _114_/X VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X471 VPWR a_12175_6549# a_11706_6727# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X472 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X473 a_11506_8751# a_11329_8751# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X474 VGND _098_/B a_9879_9117# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X475 a_13622_8751# a_13445_8751# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X476 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.05e+06u
X477 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X478 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X479 a_10862_7913# _090_/B a_10789_7913# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X480 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X481 VGND _158_/X a_13827_4943# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X482 input3/X a_2695_16911# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X483 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X484 a_12426_8207# a_12249_8207# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X485 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X486 _133_/X a_11527_8207# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X487 VPWR a_13551_8207# _148_/C VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X488 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=2.89e+06u
X489 _109_/Y _117_/A_N VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X490 VGND _090_/C a_10927_7643# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X491 a[5] _102_/X com VPWR sky130_fd_pr__pfet_01v8 ad=7.168e+11p pd=5.68e+06u as=0p ps=0u w=2.56e+06u l=150000u
X492 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X493 a_13783_6740# _093_/A VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X494 chan[10].tgate_inst/A_NOT a_14287_15279# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X495 a_9551_10004# _099_/A VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X496 VGND _148_/A chan[8].tgate_inst/A VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X497 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X498 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X499 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X500 VGND a_9819_6005# a_9459_6263# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X501 a[6] _104_/X com VPWR sky130_fd_pr__pfet_01v8 ad=7.168e+11p pd=5.68e+06u as=0p ps=0u w=2.56e+06u l=150000u
X502 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X503 VGND a_9183_8916# chan[3].tgate_inst/A_NOT VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X504 VPWR _133_/X a_11601_4943# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X505 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X506 com chan[4].tgate_inst/A a[4] VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=7.168e+11p ps=5.68e+06u w=2.56e+06u l=150000u
X507 _125_/A a_10826_6575# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X508 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=2.89e+06u
X509 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X510 VPWR _143_/X a_13091_5487# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X511 VPWR _122_/X a_11421_5639# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X512 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X513 a_11340_9411# _090_/C a_11245_9411# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X514 VGND a_13183_8207# _117_/C VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X515 a_14283_8207# _117_/A_N a_14177_8207# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X516 a_11982_5639# _158_/B VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X517 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X518 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X519 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X520 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X521 VGND _132_/C _131_/Y VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X522 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X523 com _142_/X a[5] VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=7.168e+11p ps=5.68e+06u w=2.56e+06u l=150000u
X524 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X525 VGND a_12426_8207# a_12532_8207# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X526 _139_/X a_11251_7663# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X527 a_12919_7119# _143_/X VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X528 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X529 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X530 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.05e+06u
X531 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=2.89e+06u
X532 a_9275_7815# _098_/D_N VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X533 _158_/B a_11159_4943# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X534 VGND _122_/X a_12065_6727# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X535 VGND _116_/A a_14287_15279# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X536 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X537 com chan[6].tgate_inst/A a[6] VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=7.168e+11p ps=5.68e+06u w=2.56e+06u l=150000u
X538 VPWR _148_/A a_14514_9372# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X539 VPWR a_10563_7815# _111_/A VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X540 a_11575_7338# _093_/A VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X541 a_13691_7338# _125_/A VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X542 a_11991_6005# _133_/X VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X543 a_9773_8797# _108_/A VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X544 VGND _163_/A a_14195_7663# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X545 _129_/X a_14195_5487# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X546 VPWR a_11531_5461# a_11062_5639# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X547 a_9961_9117# a_9773_8797# a_9879_9117# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X548 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X549 a_11987_9295# _148_/B a_12237_9295# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X550 a_13311_7439# _121_/A a_13211_7439# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X551 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X552 a_12175_6549# _143_/X VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X553 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X554 _109_/Y _117_/C VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X555 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X556 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X557 a_10698_7913# _098_/A VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X558 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X559 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X560 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X561 a_13977_6263# _093_/A VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X562 VGND sel[2] a_2695_16911# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X563 _090_/B a_10607_9295# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X564 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X565 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X566 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X567 a[15] chan[15].tgate_inst/A_NOT com VPWR sky130_fd_pr__pfet_01v8 ad=7.168e+11p pd=5.68e+06u as=0p ps=0u w=2.56e+06u l=150000u
X568 VPWR input3/X a_9319_7119# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X569 a_12119_7439# a_11803_7119# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X570 a_9133_6941# a_8945_6621# a_9051_6941# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X571 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X572 a_13533_5487# _154_/B a_13433_5487# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X573 VPWR _157_/A a_14103_12015# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X574 _148_/A a_11010_8751# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X575 _117_/A_N a_13126_8751# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X576 VPWR _134_/X a_11601_4943# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X577 VPWR _143_/X a_13091_6575# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X578 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X579 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X580 VPWR a_11138_6031# a_11244_6031# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X581 VGND a_1407_12015# input2/X VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X582 a_12083_8751# _117_/A_N a_12333_8751# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X583 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X584 a_8945_6621# _108_/A VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X585 a_10129_6825# _105_/A a_10057_6825# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X586 a_12219_5162# _147_/A VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X587 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X588 _148_/D_N a_11930_8207# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X589 a_9497_7913# _105_/A a_9410_7913# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X590 _107_/A a_10642_6031# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X591 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.05e+06u
X592 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X593 VPWR _098_/B a_9301_6825# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X594 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X595 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X596 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X597 a_11706_6727# _134_/X VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X598 a_9773_6621# _107_/A VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X599 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X600 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.97e+06u
X601 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X602 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X603 a_9961_9411# _098_/B a_9870_9411# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X604 VGND a_14514_9372# chan[2].tgate_inst/A VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X605 _109_/Y a_11895_8751# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X606 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X607 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.97e+06u
X608 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X609 VPWR a_14669_8797# _126_/Y VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X610 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.97e+06u
X611 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X612 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X613 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X614 a_11138_6031# a_10961_6031# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X615 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X616 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X617 VGND _117_/A_N a_13841_9615# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X618 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X619 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X620 VGND _121_/A a_13551_8207# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X621 a[9] _114_/X com VPWR sky130_fd_pr__pfet_01v8 ad=7.168e+11p pd=5.68e+06u as=0p ps=0u w=2.56e+06u l=150000u
X622 a_9551_10004# _099_/A VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X623 a_11789_8751# a_11612_8751# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X624 a_13905_8751# a_13728_8751# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X625 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X626 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X627 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X628 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.97e+06u
X629 a_11531_5461# _139_/X VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X630 a_11421_6031# a_11244_6031# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X631 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X632 a_9879_9117# a_9773_8797# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X633 a_12709_8207# a_12532_8207# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X634 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X635 _161_/A a_13257_5853# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X636 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X637 a_8631_7828# _086_/X VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X638 VGND _088_/A a_10642_6031# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X639 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X640 a_9229_6825# a_8767_6938# a_9133_6941# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X641 a_10287_5652# _144_/X VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X642 _091_/A a_11163_9411# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X643 _139_/X a_11251_7663# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X644 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=2.89e+06u
X645 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X646 VGND _148_/A a_14514_9372# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X647 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X648 _163_/A a_13091_6575# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X649 _155_/A a_12337_8029# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X650 a_11023_4564# _150_/X VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X651 a_13173_6575# _139_/X a_13091_6575# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X652 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X653 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X654 a_10034_9411# _090_/C a_9961_9411# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X655 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X656 _148_/C a_13551_8207# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X657 a_12337_8029# a_12171_7663# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X658 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X659 VGND _107_/A a_10961_6031# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X660 _098_/D_N a_9687_7119# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X661 VGND _121_/A a_10515_7122# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X662 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X663 VGND _098_/D_N a_10099_9385# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X664 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.05e+06u
X665 VGND a_12683_6549# _132_/C VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X666 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X667 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X668 com chan[3].tgate_inst/A a[3] VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=7.168e+11p ps=5.68e+06u w=2.56e+06u l=150000u
X669 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X670 VGND _139_/X a_12447_6031# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X671 chan[10].tgate_inst/A a_14103_11471# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X672 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.97e+06u
X673 a_10563_8439# _090_/C VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X674 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X675 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X676 VGND a_9278_8439# a_9227_8207# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X677 VPWR a_11999_7119# a_12119_7439# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X678 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X679 VGND a_14296_6263# _093_/A VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X680 VPWR _161_/A a_14103_6575# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X681 VGND _157_/A a_14103_12015# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X682 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X683 VGND a_11322_6575# a_11428_6575# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X684 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X685 a_12219_5162# _147_/A VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X686 VPWR _111_/A a_10055_15823# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X687 VGND _132_/A _130_/Y VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X688 a_9735_9527# a_10099_9385# a_10034_9411# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X689 a_14032_9615# _148_/B VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X690 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X691 _124_/Y _132_/C VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X692 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.97e+06u
X693 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X694 a_11271_5533# _158_/B VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X695 VPWR a_11575_7338# _090_/C VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X696 VPWR a_13691_7338# _143_/X VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X697 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.05e+06u
X698 a_11835_6351# _134_/X a_11731_6351# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X699 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X700 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X701 _124_/Y a_14025_8797# a_14407_8751# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X702 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X703 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X704 a_13361_5487# a_13091_5487# a_13257_5853# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X705 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X706 VPWR _117_/A_N a_12654_9269# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X707 a_10881_7119# a_10693_7439# a_10799_7119# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X708 a_11413_9411# _090_/B a_11340_9411# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X709 _101_/X a_9879_6941# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X710 a_12683_6549# _122_/X VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X711 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.97e+06u
X712 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X713 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=2.89e+06u
X714 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.05e+06u
X715 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=2.89e+06u
X716 VPWR a_13694_6263# a_13643_6031# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X717 VPWR a_11522_6263# _137_/A VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X718 a_13341_7913# _132_/C a_13538_7913# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X719 a_12434_9295# _132_/C a_12237_9295# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X720 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X721 VPWR _107_/A a_12815_8207# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X722 a_12441_7663# a_12171_7663# a_12337_8029# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X723 VPWR a_10471_6740# _098_/B VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X724 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X725 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X726 _157_/A a_13039_7439# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X727 _147_/A a_11601_4943# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X728 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X729 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X730 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X731 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X732 VPWR _154_/B a_13039_7439# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X733 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X734 a_8631_7828# _086_/X VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X735 a[14] _126_/Y com VPWR sky130_fd_pr__pfet_01v8 ad=7.168e+11p pd=5.68e+06u as=0p ps=0u w=2.56e+06u l=150000u
X736 a_14407_8751# _148_/B a_14300_8751# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X737 _140_/X a_10147_7119# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X738 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.05e+06u
X739 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X740 a_11999_7119# _133_/X VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X741 a_10287_5652# _144_/X VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X742 VGND _132_/A a_13758_7637# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X743 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.05e+06u
X744 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X745 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.05e+06u
X746 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.97e+06u
X747 a[10] chan[10].tgate_inst/A_NOT com VPWR sky130_fd_pr__pfet_01v8 ad=7.168e+11p pd=5.68e+06u as=0p ps=0u w=2.56e+06u l=150000u
X748 a_12213_10089# a_12143_9991# chan[8].tgate_inst/A VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X749 VGND a_12654_9269# chan[4].tgate_inst/A VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X750 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=2.89e+06u
X751 _142_/X a_10791_4943# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X752 a_12395_5533# a_12341_5639# a_12295_5533# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X753 VPWR a_2695_16911# input3/X VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X754 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X755 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X756 VPWR _158_/B a_12613_6031# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X757 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X758 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X759 a_13091_7913# _117_/C VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X760 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=2.89e+06u
X761 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X762 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X763 a_13694_6263# a_13790_6005# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X764 a_12237_9295# _148_/B a_11987_9295# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X765 _148_/B a_12815_8207# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X766 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X767 com _131_/Y a[1] VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=7.168e+11p ps=5.68e+06u w=2.56e+06u l=150000u
X768 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.97e+06u
X769 VPWR _140_/X a_11062_5639# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X770 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X771 VGND _125_/A a_10563_7815# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X772 com _161_/X a[14] VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=7.168e+11p ps=5.68e+06u w=2.56e+06u l=150000u
X773 _158_/X a_12613_6031# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X774 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X775 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X776 a_10563_8439# _098_/A VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X777 a_11667_7828# _093_/A VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X778 a_13125_7439# a_12723_7119# a_13039_7439# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X779 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X780 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X781 _109_/Y _148_/B VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X782 a_14093_8207# _148_/A VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X783 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X784 VGND sel[1] a_1407_12015# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X785 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X786 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X787 a_9569_7913# a_9539_7815# a_9497_7913# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X788 VPWR _098_/A a_11251_7663# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X789 VGND _161_/A a_14103_6575# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X790 VGND _111_/A a_10055_15823# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X791 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X792 a_14300_8751# _148_/C a_14216_8751# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X793 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.97e+06u
X794 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X795 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X796 VGND a_10195_8426# _090_/D_N VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X797 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X798 _129_/A a_10515_5737# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X799 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X800 VGND _134_/X a_12391_7439# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X801 _088_/A a_10055_2223# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X802 VPWR _122_/X a_12065_6727# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X803 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X804 VGND _117_/C a_12523_8751# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X805 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X806 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X807 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X808 VGND _148_/A chan[4].tgate_inst/A VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X809 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X810 VPWR input3/X a_9687_7119# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X811 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X812 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X813 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X814 VPWR a_9275_7815# _097_/A VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X815 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X816 a_11522_6263# a_11991_6005# a_11935_6351# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X817 a_9723_6121# _152_/C VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X818 VGND a_11706_6727# _144_/X VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X819 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=2.89e+06u
X820 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X821 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X822 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X823 a_13257_5853# _154_/B VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X824 VGND a_9595_6938# a_9879_6941# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X825 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X826 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X827 VGND a_10471_6740# _098_/B VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X828 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X829 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X830 _119_/A a_10799_7119# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X831 VPWR _116_/A a_14287_15279# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X832 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X833 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X834 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X835 _116_/A a_9879_9117# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X836 VPWR _148_/C chan[15].tgate_inst/A_NOT VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X837 a[13] _124_/Y com VPWR sky130_fd_pr__pfet_01v8 ad=7.168e+11p pd=5.68e+06u as=0p ps=0u w=2.56e+06u l=150000u
X838 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X839 a_12337_8029# _134_/X VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X840 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X841 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X842 VGND _098_/B a_9735_9527# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X843 _132_/C a_12683_6549# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X844 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X845 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X846 _142_/X a_10791_4943# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X847 VPWR a_13977_6263# a_13790_6005# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X848 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X849 a_9051_6941# _105_/A VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X850 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X851 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X852 _153_/A a_12119_7439# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X853 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.97e+06u
X854 a_11982_5639# a_12451_5461# a_12395_5533# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X855 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X856 VGND _105_/A a_9595_9114# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X857 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X858 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X859 a_14693_9295# _132_/A VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X860 a_9278_8439# a_9374_8181# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X861 a_9870_9411# _098_/A VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X862 VPWR _148_/A a_11987_9295# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X863 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X864 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X865 a_11163_9411# _090_/B VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X866 a_10563_8439# a_10927_8297# a_10862_8323# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X867 a_11322_6575# a_11145_6575# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X868 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X869 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X870 a_11667_7828# _093_/A VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X871 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X872 a_13211_7439# a_12919_7119# a_13125_7439# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X873 VPWR _122_/X a_11435_4943# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X874 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X875 a_14944_8751# _148_/D_N a_14860_8751# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X876 a_9183_6250# _103_/X VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X877 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=2.89e+06u
X878 VGND _098_/A a_11251_7663# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X879 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X880 _132_/A a_13183_9295# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X881 a_10057_9001# a_9595_9114# a_9961_9117# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X882 a_9539_7815# _108_/A VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X883 VPWR _121_/A a_8767_6938# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X884 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X885 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X886 a_10693_7439# _152_/C VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X887 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X888 VGND _134_/X a_11877_5309# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X889 a_13433_5487# _140_/X a_13361_5487# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X890 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X891 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=2.89e+06u
X892 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X893 VPWR _121_/A a_9595_6938# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X894 _088_/A a_10055_2223# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X895 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X896 a_13091_6575# _139_/X VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X897 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X898 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X899 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X900 a_9880_8439# input2/X VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X901 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X902 a_12613_6031# a_12447_6031# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X903 a_11522_6263# a_11881_6263# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X904 a_9183_6250# _103_/X VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X905 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X906 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X907 a_11057_9411# _090_/D_N VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X908 VPWR a_13841_9615# chan[11].tgate_inst/A_NOT VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X909 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X910 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X911 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X912 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X913 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X914 VPWR a_13183_8207# _117_/C VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X915 com chan[8].tgate_inst/A a[8] VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=7.168e+11p ps=5.68e+06u w=2.56e+06u l=150000u
X916 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X917 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X918 a_12613_7663# _134_/X a_12513_7663# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X919 _133_/X a_11527_8207# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X920 _131_/Y _148_/C VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X921 VPWR a_12426_8207# a_12532_8207# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X922 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X923 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X924 VPWR a_11322_6575# a_11428_6575# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X925 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X926 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X927 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X928 VPWR a_10563_8916# _098_/A VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X929 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X930 a_11023_4564# _150_/X VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X931 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X932 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X933 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=2.89e+06u
X934 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X935 VPWR a_9551_10004# chan[4].tgate_inst/A_NOT VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X936 VGND a_10927_7643# a_10563_7815# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X937 _103_/X a_9051_6941# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X938 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X939 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X940 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X941 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X942 chan[12].tgate_inst/A_NOT a_12723_12559# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X943 chan[10].tgate_inst/A a_14103_11471# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X944 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X945 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X946 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X947 a_13257_5853# a_13091_5487# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X948 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X949 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X950 a_12889_6397# _140_/X a_12789_6397# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X951 com _151_/X a[9] VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=7.168e+11p ps=5.68e+06u w=2.56e+06u l=150000u
X952 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X953 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X954 a_10789_8323# _090_/B a_10698_8323# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X955 VPWR a_13783_6740# _122_/X VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X956 a_10799_7119# _098_/B VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X957 a_9681_6147# _098_/D_N a_9594_6147# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X958 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.97e+06u
X959 VGND a_10563_8916# _098_/A VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X960 _131_/Y a_13758_7637# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X961 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.05e+06u
X962 _121_/A a_9319_7119# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X963 VGND _143_/X a_11803_7119# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X964 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X965 VGND _105_/A a_9275_7815# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X966 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X967 VPWR _117_/C chan[11].tgate_inst/A_NOT VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X968 _134_/X a_11251_8207# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X969 VPWR _121_/A a_10515_7122# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X970 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X971 VGND _122_/X a_11881_6263# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X972 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X973 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X974 VPWR _117_/C chan[15].tgate_inst/A_NOT VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X975 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X976 _132_/A a_13183_9295# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X977 _126_/Y _117_/A_N VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X978 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X979 VPWR _117_/A_N a_13445_8751# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X980 a_13091_7913# _148_/C a_13341_7913# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X981 VGND input3/X a_9319_7119# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X982 VGND a_11062_5639# _142_/A VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X983 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.05e+06u
X984 com chan[15].tgate_inst/A a[15] VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=7.168e+11p ps=5.68e+06u w=2.56e+06u l=150000u
X985 VPWR _117_/C _109_/Y VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X986 a_9561_8439# _108_/A VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X987 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X988 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X989 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.05e+06u
X990 VGND _153_/A a_14103_11471# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X991 VPWR _121_/A a_13551_8207# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X992 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X993 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X994 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X995 VGND _133_/X a_12341_5639# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X996 a_11605_6575# a_11428_6575# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X997 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X998 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X999 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X1000 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1001 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1002 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1003 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1004 a_14873_9295# _132_/C a_14777_9295# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1005 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X1006 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.05e+06u
X1007 a_12297_10089# _148_/C a_12213_10089# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1008 a_12333_8751# _148_/B a_12523_8751# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1009 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1010 VPWR _129_/A a_14195_5487# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X1011 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X1012 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1013 _131_/Y _117_/C VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1014 a_10977_7235# a_10515_7122# a_10881_7119# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1015 VPWR a_11982_5639# _150_/X VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1016 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1017 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X1018 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.05e+06u
X1019 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1020 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X1021 VPWR _117_/A_N a_13841_9615# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1022 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X1023 VPWR _088_/A a_11159_4943# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X1024 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X1025 VGND _148_/B chan[4].tgate_inst/A VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1026 VPWR input3/X a_10607_9295# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X1027 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1028 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X1029 VPWR sel[0] a_10055_2223# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X1030 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1031 a_9879_9117# _098_/D_N VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1032 a[7] _109_/Y com VPWR sky130_fd_pr__pfet_01v8 ad=7.168e+11p pd=5.68e+06u as=0p ps=0u w=2.56e+06u l=150000u
X1033 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.97e+06u
X1034 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1035 VPWR a_11895_8751# _109_/Y VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1036 _148_/C a_13551_8207# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1037 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X1038 a_8945_6621# _108_/A VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1039 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.05e+06u
X1040 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1041 VGND _107_/A a_9635_7637# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1042 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X1043 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.05e+06u
X1044 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1045 a_11731_6351# _158_/B VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1046 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1047 VGND a_9880_8439# _108_/A VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1048 a_13538_7913# a_13758_7637# _131_/Y VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1049 a_9773_6621# _107_/A VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1050 VGND _090_/D_N a_9879_6941# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1051 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X1052 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.97e+06u
X1053 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1054 VGND a_10563_8439# _086_/X VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1055 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X1056 chan[12].tgate_inst/A_NOT a_12723_12559# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X1057 VGND a_10515_7122# a_10799_7119# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1058 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1059 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.97e+06u
X1060 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1061 a_10693_7439# _152_/C VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1062 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1063 VGND _117_/A_N a_13445_8751# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1064 com _147_/X a[7] VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=7.168e+11p ps=5.68e+06u w=2.56e+06u l=150000u
X1065 VPWR _090_/B a_11527_8207# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X1066 VPWR _090_/C a_10927_7643# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1067 a_12295_5533# _154_/B a_12191_5533# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1068 VGND a_13783_6740# _122_/X VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X1069 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X1070 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1071 VGND a_9551_10004# chan[4].tgate_inst/A_NOT VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X1072 VGND _090_/C a_11163_9411# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1073 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X1074 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1075 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X1076 _154_/B a_11895_7663# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X1077 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1078 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X1079 a_13039_7439# _121_/A VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1080 VGND a_9459_6263# _113_/X VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1081 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X1082 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.97e+06u
X1083 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1084 VPWR _090_/B a_13126_8751# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1085 a_9459_6263# a_9723_6121# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1086 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=2.89e+06u
X1087 a_14261_7439# _132_/A a_14177_7439# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1088 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.05e+06u
X1089 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1090 a_11991_6005# _133_/X VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1091 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X1092 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X1093 a_12717_6397# a_12447_6031# a_12613_6031# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1094 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.05e+06u
X1095 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.05e+06u
X1096 input3/X a_2695_16911# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1097 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X1098 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X1099 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1100 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1101 a_9275_7815# a_9635_7637# a_9569_7913# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1102 a_12451_5461# _139_/X VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1103 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X1104 VGND _148_/A a_14025_8797# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1105 a_10195_8426# input2/X VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X1106 VGND _148_/D_N _130_/Y VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1107 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1108 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1109 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.97e+06u
X1110 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.05e+06u
X1111 a_11506_8751# a_11329_8751# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1112 a_13622_8751# a_13445_8751# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1113 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.05e+06u
X1114 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X1115 VPWR _098_/D_N a_10099_9385# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1116 VGND a_11575_7338# _090_/C VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X1117 a_11062_5639# _158_/B VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1118 _107_/A a_10642_6031# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1119 VGND a_13691_7338# _143_/X VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X1120 VGND _129_/A a_14195_5487# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X1121 VGND a_11138_6031# a_11244_6031# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1122 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1123 VGND _088_/A a_11159_4943# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X1124 _126_/Y _117_/C VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1125 _130_/Y _117_/A_N VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1126 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X1127 a_11999_7119# _133_/X VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1128 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1129 a_9735_5162# _113_/X VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X1130 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.97e+06u
X1131 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.05e+06u
X1132 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X1133 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1134 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1135 VGND input3/X a_10607_9295# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X1136 a_12333_8751# _117_/A_N a_12083_8751# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1137 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X1138 VGND sel[0] a_10055_2223# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X1139 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1140 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1141 _159_/X a_13827_4943# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X1142 a_11138_6031# a_10961_6031# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1143 a_12019_6621# _140_/X a_11915_6621# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1144 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1145 a_7987_6740# _137_/A VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X1146 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1147 a_12391_7439# _152_/C a_12291_7439# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1148 VPWR _152_/C a_12613_6031# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1149 VGND a_9561_8439# a_9374_8181# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1150 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1151 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X1152 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X1153 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.05e+06u
X1154 a_12393_10089# _148_/B a_12297_10089# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1155 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.97e+06u
X1156 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X1157 a_13538_7913# _132_/C a_13341_7913# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1158 VPWR a_12219_5162# _147_/X VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X1159 a_13231_6250# _093_/A VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X1160 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1161 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1162 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1163 chan[4].tgate_inst/A a_12654_9269# a_12434_9295# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1164 a_10862_8323# _090_/C a_10789_8323# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1165 _140_/X a_10147_7119# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X1166 a_9753_6147# a_9723_6121# a_9681_6147# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1167 VGND _090_/B a_13126_8751# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1168 a_9410_7913# _098_/D_N VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1169 VGND _122_/X a_11435_4943# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1170 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.05e+06u
X1171 a_9301_6825# _105_/A a_9229_6825# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1172 _102_/X a_10055_3311# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X1173 VPWR _098_/B a_10129_9001# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1174 VGND _088_/A a_10826_6575# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1175 a_11601_4943# _158_/B VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1176 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1177 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1178 VPWR _154_/B a_12337_8029# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1179 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X1180 a_11982_5639# a_12341_5639# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1181 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X1182 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1183 VGND _117_/A_N a_12654_9269# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1184 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1185 _154_/B a_11895_7663# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X1186 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1187 VGND a_9635_7637# a_9275_7815# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1188 a_13231_6250# _093_/A VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X1189 a_9459_6263# _090_/D_N VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1190 chan[2].tgate_inst/A_NOT a_14103_10927# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X1191 com _159_/X a[13] VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=7.168e+11p ps=5.68e+06u w=2.56e+06u l=150000u
X1192 a_14093_7439# _132_/C VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1193 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1194 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1195 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1196 VGND _132_/C chan[4].tgate_inst/A VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1197 VPWR a_9735_9527# _099_/A VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1198 chan[15].tgate_inst/A a_14195_7663# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X1199 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X1200 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X1201 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1202 VPWR a_9278_8439# a_9227_8207# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1203 VPWR _090_/C a_11895_7663# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X1204 VPWR a_14296_6263# _093_/A VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1205 a_9879_6941# a_9773_6621# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1206 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1207 com chan[11].tgate_inst/A a[11] VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=7.168e+11p ps=5.68e+06u w=2.56e+06u l=150000u
X1208 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X1209 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X1210 VPWR a_8631_7828# _087_/X VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X1211 a_9594_6147# _090_/D_N VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1212 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1213 a_10563_7815# _090_/B VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1214 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1215 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1216 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1217 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1218 _148_/A a_11010_8751# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1219 _117_/A_N a_13126_8751# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1220 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1221 VPWR _148_/B _109_/Y VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1222 a_14177_8207# _132_/A a_14093_8207# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1223 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1224 VGND a_11023_4564# _151_/X VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X1225 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1226 _125_/A a_10826_6575# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1227 VGND _090_/D_N a_10515_5737# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1228 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X1229 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1230 VPWR input2/X a_11251_8207# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X1231 VPWR _107_/A a_12337_8029# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1232 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.05e+06u
X1233 chan[2].tgate_inst/A_NOT a_14103_10927# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X1234 VGND input3/X a_9687_7119# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X1235 chan[8].tgate_inst/A_NOT a_10055_15823# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X1236 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1237 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1238 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1239 a_9183_8916# _097_/A VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X1240 a_9735_5162# _113_/X VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X1241 VPWR _134_/X a_11522_6263# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1242 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1243 a_10698_8323# _098_/A VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1244 VPWR _132_/A a_13758_7637# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1245 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1246 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1247 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.97e+06u
X1248 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X1249 _109_/Y a_11895_8751# a_12083_8751# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1250 a_12523_8751# _117_/C VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1251 _159_/X a_13827_4943# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X1252 a[11] chan[11].tgate_inst/A_NOT com VPWR sky130_fd_pr__pfet_01v8 ad=7.168e+11p pd=5.68e+06u as=0p ps=0u w=2.56e+06u l=150000u
X1253 chan[8].tgate_inst/A a_12143_9991# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1254 chan[4].tgate_inst/A _148_/B VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1255 _126_/Y a_14669_8797# a_15051_8751# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1256 a_12513_7663# _154_/B a_12441_7663# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1257 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1258 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1259 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1260 VGND a_10563_7815# _111_/A VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1261 a_7987_6740# _137_/A VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X1262 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1263 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=1.05e+06u
X1264 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X1265 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1266 a_9278_8439# a_9374_8181# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1267 a_14296_6263# sel[3] VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1268 a_11789_8751# a_11612_8751# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1269 a_13905_8751# a_13728_8751# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1270 VPWR _107_/A a_9635_7637# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1271 VGND a_12219_5162# _147_/X VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X1272 VPWR _133_/X a_12341_5639# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1273 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1274 _157_/A a_13039_7439# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1275 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=590000u
X1276 input2/X a_1407_12015# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1277 a_12237_9295# _132_/C a_12434_9295# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1278 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1279 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1280 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1281 a[0] _129_/X com VPWR sky130_fd_pr__pfet_01v8 ad=7.168e+11p pd=5.68e+06u as=0p ps=0u w=2.56e+06u l=150000u
X1282 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1283 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1284 a_9183_8916# _097_/A VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X1285 _163_/A a_13091_6575# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1286 VGND a_12815_8207# _148_/B VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1287 a_12119_6621# a_12065_6727# a_12019_6621# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1288 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.05e+06u
X1289 a_9773_8797# _108_/A VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1290 VPWR _117_/A_N _109_/Y VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1291 _102_/X a_10055_3311# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X1292 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1293 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1294 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1295 VPWR sel[2] a_2695_16911# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1296 chan[11].tgate_inst/A_NOT _148_/D_N VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1297 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1298 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=2.89e+06u
X1299 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1300 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1301 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1302 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1303 VPWR a_12451_5461# a_11982_5639# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1304 a_12789_6397# _158_/B a_12717_6397# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1305 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1306 VPWR a_12683_6549# _132_/C VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1307 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1308 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=590000u
X1309 VGND _098_/B a_10927_8297# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1310 a_12426_8207# a_12249_8207# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=650000u l=150000u
X1311 VPWR _119_/A a_12723_12559# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X1312 VPWR _153_/A a_14103_11471# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
X1313 a_9880_8439# input2/X VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1314 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1315 a_9459_6263# a_9819_6005# a_9753_6147# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1316 VGND _098_/B a_9051_6941# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1317 com _130_/Y a[0] VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=7.168e+11p ps=5.68e+06u w=2.56e+06u l=150000u
X1318 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1319 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1320 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=1.05e+06u
X1321 _158_/X a_12613_6031# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1322 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1323 a_14777_9295# _148_/C a_14693_9295# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=1e+06u l=150000u
X1324 chan[15].tgate_inst/A a_14195_7663# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X1325 a_13039_7439# a_12723_7119# VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1326 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1327 a_13694_6263# a_13790_6005# VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1328 VPWR VGND VPWR VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=870000u l=4.73e+06u
X1329 VGND _125_/A a_11163_9411# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1330 VGND _090_/C a_11895_7663# VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X1331 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1332 VGND VPWR VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=550000u l=4.73e+06u
X1333 VGND a_8631_7828# _087_/X VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=520000u l=150000u
X1334 a_10563_7815# _098_/A VGND VGND sky130_fd_pr__nfet_01v8 ad=0p pd=0u as=0p ps=0u w=420000u l=150000u
X1335 VPWR input3/X a_10147_7119# VPWR sky130_fd_pr__pfet_01v8_hvt ad=0p pd=0u as=0p ps=0u w=790000u l=150000u
C0 VPWR a[1] 10.50fF
C1 _109_/Y VPWR 12.07fF
C2 _117_/A_N _148_/D_N 2.66fF
C3 _093_/A VPWR 4.03fF
C4 _090_/C _090_/B 2.28fF
C5 _122_/X _158_/B 2.62fF
C6 VPWR a[15] 6.06fF
C7 VPWR chan[10].tgate_inst/A_NOT 2.28fF
C8 VPWR a[11] 2.35fF
C9 VPWR _143_/X 6.48fF
C10 _098_/D_N VPWR 6.24fF
C11 a[15] com 2.16fF
C12 VPWR _090_/D_N 5.68fF
C13 VPWR _114_/X 9.16fF
C14 VPWR _133_/X 3.99fF
C15 a[3] VPWR 4.59fF
C16 _117_/A_N _117_/C 3.03fF
C17 _090_/C VPWR 5.00fF
C18 _148_/B _148_/A 2.49fF
C19 VPWR _090_/B 7.69fF
C20 _108_/A chan[4].tgate_inst/A 3.25fF
C21 _155_/A VPWR 2.94fF
C22 _139_/X VPWR 6.58fF
C23 VPWR chan[12].tgate_inst/A 2.24fF
C24 _152_/C VPWR 8.30fF
C25 VPWR chan[11].tgate_inst/A 4.02fF
C26 _121_/A VPWR 7.36fF
C27 _134_/X _133_/X 2.03fF
C28 VPWR _132_/A 6.02fF
C29 a[3] com 4.79fF
C30 _132_/C _148_/C 2.38fF
C31 _148_/C _148_/D_N 2.22fF
C32 _159_/X VPWR 2.94fF
C33 _152_/C _134_/X 2.01fF
C34 _098_/A _105_/A 2.08fF
C35 VPWR a[13] 7.12fF
C36 VPWR input3/X 25.33fF
C37 _126_/Y VPWR 3.02fF
C38 VPWR _151_/X 10.28fF
C39 VPWR _157_/A 4.03fF
C40 VPWR _134_/X 4.33fF
C41 VPWR com 35.74fF
C42 a[8] VPWR 8.72fF
C43 _117_/C _148_/C 2.07fF
C44 VPWR _091_/A 6.28fF
C45 VPWR _142_/X 2.58fF
C46 chan[6].tgate_inst/A a[14] 5.53fF
C47 VPWR _130_/Y 4.34fF
C48 VPWR _129_/A 2.81fF
C49 a[3] a[4] 2.05fF
C50 sel[3] VPWR 3.79fF
C51 _098_/D_N _107_/A 2.02fF
C52 _122_/X VPWR 2.81fF
C53 VPWR a[12] 7.09fF
C54 chan[8].tgate_inst/A VPWR 4.77fF
C55 VPWR _148_/A 11.02fF
C56 _139_/X _108_/A 2.37fF
C57 VPWR _116_/A 9.22fF
C58 _117_/A_N _148_/B 2.29fF
C59 VPWR a[6] 6.09fF
C60 _132_/C VPWR 8.74fF
C61 VPWR _148_/D_N 5.78fF
C62 a[4] VPWR 7.75fF
C63 _088_/A VPWR 5.47fF
C64 _124_/Y VPWR 7.23fF
C65 _108_/A VPWR 8.20fF
C66 _124_/Y _126_/Y 2.43fF
C67 _087_/X chan[4].tgate_inst/A 2.06fF
C68 a[7] VPWR 11.72fF
C69 a[4] com 2.07fF
C70 _147_/X VPWR 4.04fF
C71 _107_/A VPWR 4.03fF
C72 VPWR _125_/A 9.93fF
C73 VPWR _117_/C 8.04fF
C74 VPWR _119_/A 4.19fF
C75 VPWR chan[12].tgate_inst/A_NOT 2.23fF
C76 VPWR _105_/A 9.22fF
C77 _101_/X VPWR 2.41fF
C78 _155_/A _153_/A 2.48fF
C79 VPWR chan[11].tgate_inst/A_NOT 5.84fF
C80 _129_/X VPWR 2.42fF
C81 _086_/X VPWR 3.41fF
C82 input2/X _109_/Y 2.19fF
C83 _131_/Y VPWR 24.94fF
C84 _153_/A VPWR 3.33fF
C85 chan[11].tgate_inst/A_NOT com 4.74fF
C86 VPWR _140_/X 6.34fF
C87 _129_/X com 2.30fF
C88 chan[6].tgate_inst/A VPWR 9.46fF
C89 VPWR _104_/X 4.93fF
C90 _111_/A VPWR 5.43fF
C91 _117_/A_N VPWR 4.93fF
C92 _111_/A input3/X 6.35fF
C93 VPWR a[9] 9.25fF
C94 VPWR a[2] 5.08fF
C95 _088_/A _147_/X 2.20fF
C96 VPWR _154_/B 2.78fF
C97 VPWR a[14] 23.78fF
C98 VPWR chan[4].tgate_inst/A 9.27fF
C99 chan[10].tgate_inst/A VPWR 6.57fF
C100 _158_/B VPWR 2.28fF
C101 _093_/A _143_/X 2.16fF
C102 VPWR _098_/B 6.60fF
C103 chan[4].tgate_inst/A_NOT VPWR 4.66fF
C104 VPWR _098_/A 4.04fF
C105 _161_/A VPWR 2.30fF
C106 input2/X VPWR 8.71fF
C107 _122_/X _140_/X 3.88fF
C108 _163_/A VPWR 2.17fF
C109 _148_/C _132_/A 2.37fF
C110 VPWR _087_/X 4.06fF
C111 a[5] com 4.43fF
C112 chan[3].tgate_inst/A_NOT VPWR 6.06fF
C113 VPWR _148_/B 3.21fF
C114 VPWR _158_/X 3.27fF
C115 _107_/A _105_/A 2.20fF
C116 VPWR chan[3].tgate_inst/A 4.46fF
C117 VPWR _148_/C 3.50fF
C118 chan[6].tgate_inst/A a[6] 5.01fF
C119 a[0] VGND 3.82fF
C120 a[5] VGND 5.19fF
C121 a[13] VGND 12.04fF
C122 _151_/X VGND 4.83fF 
C123 a[9] VGND 8.51fF
C124 _147_/X VGND 6.30fF 
C125 _129_/X VGND 3.22fF 
C126 a[14] VGND 7.77fF
C127 sel[3] VGND 5.52fF
C128 _158_/B VGND 3.41fF 
C129 chan[6].tgate_inst/A VGND 6.40fF 
C130 a[6] VGND 5.40fF
C131 _104_/X VGND 3.28fF 
C132 _161_/X VGND 3.39fF 
C133 _122_/X VGND 4.37fF 
C134 _088_/A VGND 3.23fF 
C135 _137_/A VGND 3.07fF 
C136 _140_/X VGND 3.67fF 
C137 a[15] VGND 7.09fF
C138 chan[15].tgate_inst/A_NOT VGND 2.26fF 
C139 chan[15].tgate_inst/A VGND 2.31fF 
C140 _131_/Y VGND 2.62fF 
C141 _154_/B VGND 5.54fF 
C142 _093_/A VGND 2.25fF 
C143 a[1] VGND 2.62fF
C144 _087_/X VGND 3.81fF 
C145 _130_/Y VGND 7.85fF 
C146 _134_/X VGND 2.52fF 
C147 _107_/A VGND 6.24fF 
C148 _126_/Y VGND 3.20fF 
C149 _124_/Y VGND 3.04fF 
C150 _109_/Y VGND 8.62fF 
C151 _105_/A VGND 2.45fF 
C152 _117_/A_N VGND 2.39fF 
C153 chan[3].tgate_inst/A VGND 8.31fF 
C154 _117_/C VGND 3.77fF 
C155 _090_/D_N VGND 3.96fF 
C156 a[3] VGND 7.62fF
C157 chan[3].tgate_inst/A_NOT VGND 3.04fF 
C158 _148_/D_N VGND 4.08fF 
C159 _148_/B VGND 5.91fF 
C160 _148_/C VGND 5.35fF 
C161 _099_/A VGND 2.14fF 
C162 chan[4].tgate_inst/A VGND 12.35fF 
C163 a[4] VGND 11.97fF
C164 chan[4].tgate_inst/A_NOT VGND 8.58fF 
C165 chan[2].tgate_inst/A VGND 2.87fF 
C166 a[2] VGND 4.46fF
C167 input2/X VGND 10.30fF 
C168 a[12] VGND 7.79fF
C169 chan[12].tgate_inst/A_NOT VGND 5.05fF 
C170 chan[11].tgate_inst/A VGND 5.24fF 
C171 a[11] VGND 3.92fF
C172 chan[11].tgate_inst/A_NOT VGND 3.35fF 
C173 chan[10].tgate_inst/A VGND 5.03fF 
C174 com VGND 34.55fF
C175 a[10] VGND 2.81fF
C176 a[8] VGND 6.69fF
C177 chan[8].tgate_inst/A_NOT VGND 2.18fF 
C178 VPWR VGND 1662.16fF"}
C {devices/iopin.sym} -310 -140 0 0 {name=p1 lab=VPWR}
C {devices/iopin.sym} -310 -110 0 0 {name=p2 lab=VGND}
C {devices/iopin.sym} -310 -80 0 0 {name=p3 lab=com}
C {devices/ipin.sym} -260 -40 0 0 {name=p4 lab=sel[0]}
C {devices/ipin.sym} -260 -20 0 0 {name=p5 lab=sel[1] dir=input}
C {devices/ipin.sym} -260 20 0 0 {name=p6 lab=sel[2]}
C {devices/ipin.sym} -260 40 0 0 {name=p7 lab=sel[3]}
