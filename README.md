# Caravel Analog User

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0) [![CI](https://github.com/efabless/caravel_user_project_analog/actions/workflows/user_project_ci.yml/badge.svg)](https://github.com/efabless/caravel_user_project_analog/actions/workflows/user_project_ci.yml) [![Caravan Build](https://github.com/efabless/caravel_user_project_analog/actions/workflows/caravan_build.yml/badge.svg)](https://github.com/efabless/caravel_user_project_analog/actions/workflows/caravan_build.yml)

---


The focus of this tape-out is integrating analog synapses. Specifically, we are integrating ReRAM based synapse and FG-based synapse. ReRAM based array is a 1T-1R strucuture with the goal of increasing the size of the array. FG synapses is built using the high voltage transistors present on the SKY130 process. 

